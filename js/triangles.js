var width = 1850;
var height = 940;

var margin = {top: 10, right: 30, bottom: 80, left: 40};

var w = 90;
var h = 90;

var svg = d3.select("body")
			.append("svg")
			.attr("width", width)
			.attr("height", height)
			.style("margin-left", margin.left)
			.style("margin-top", margin.top);

//const colors = ["#e49034", "#455097", "#d55029", "#3d94cf", "#d7548f"];

//--------------------------------------- Making the grid --------------------------------
var Enable_grid = true;

if (Enable_grid) {

	var xScale = d3.scaleLinear()
	             //accepts
	             .domain([0, 20])
	             //outputs
	             .range([0, w*20]);

	var yScale = d3.scaleLinear()
	             //accepts
	             .domain([0, 10])
	             //outputs
	             .range([0, h*10]);

	//Prepare axes
	var xAxisBottom = d3.axisBottom(xScale).ticks(9); 
	var yAxisRight = d3.axisRight(yScale).ticks(9);       

	//Draw axes
	svg.append("g")
	.attr("class", "axis")
	.call(xAxisBottom);

	svg.append("g")
	.attr("class", "axis")   
	.call(yAxisRight);

	//Prepare grid
	var ygridlines = d3.axisTop()
	                 .tickFormat("")
	                 .tickSize(-h*10)
	                 .ticks(18)
	                 .scale(xScale);

	var xgridlines = d3.axisLeft()
	                 .tickFormat("")
	                 .tickSize(-w*20)
	                 .ticks(9)
	                 .scale(yScale);

	//Draw grid
	svg.append("g")
	.attr("class", "main-grid")
	.call(ygridlines);

	svg.append("g")
	.attr("class", "main-grid")
	.call(xgridlines);              
}
//--------------------------------------- Main Function ----------------------------------
var data = d3.json("data/data.json").then(function(data){
				var data_array = data;
				data_array.forEach(add_shapes); 
			});

//--------------------------------------- Elementary shapes ------------------------------
function triangle(svg, object, x1, y1, x2, y2, x3, y3){
	var tri = svg.append("polygon")	
				.attr("points", `${x1},${y1} ${x2},${y2} ${x3},${y3}`)
		  		.attr("fill", object.before_colour);


	var shape = `${x1},${y1} ${x2},${y2} ${x3},${y3}`;

	tri.on('mouseover', function() {
		d3.select(this).style("cursor", "pointer");
    })
	.on('mouseout', function() {
        d3.select(this).style("cursor", "default");
    })
	.on('click', function(){
		d3.select(this).style('fill', object.after_colour);
		loadPage(object, shape);
	});
}

function rectangle(svg, object, x1, y1, x2, y2, x3, y3, x4, y4){
	var rect = svg.append("polygon")
				.attr("points", `${x1},${y1} ${x2},${y2} ${x3},${y3} ${x4},${y4}`)
		  		.attr("fill", object.before_colour)
		  		.attr("stroke", "none");

	var shape = `${x1},${y1} ${x2},${y2} ${x3},${y3} ${x4},${y4}`;

	rect.on('mouseover', function() {
	    d3.select(this).style("cursor", "pointer");
    })
    .on('mouseout', function() {
	    d3.select(this).style("cursor", "default");
	})
	.on('click', function(){
	  	d3.select(this).style('fill', object.after_colour);
		loadPage(object, shape);
	});
}

function hexagon(svg, object, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6){
	var hex = svg.append("polygon")
				.attr("points", `${x1},${y1} ${x2},${y2} ${x3},${y3} ${x4},${y4} ${x5},${y5} ${x6},${y6}`)
		  		.attr("fill", object.before_colour)
		  		.attr("stroke", "none");

	var shape = `${x1},${y1} ${x2},${y2} ${x3},${y3} ${x4},${y4} ${x5},${y5} ${x6},${y6}`;
  	
  	hex.on('mouseover', function() {
	    d3.select(this).style("cursor", "pointer");
    })
    .on('mouseout', function() {
	    d3.select(this).style("cursor", "default");
	})
	.on('click', function(){
	  	d3.select(this).style('fill', object.after_colour);
		loadPage(object);
	});
}

//------------------------------------------- Shapes --------------------------------------
function triangle_up(svg, object, x, y, w, h) {
  	triangle(svg, object, x, y + h, x + w, y + h, x + w/2, y);
}

function triangle_down(svg, object, x, y, w, h) {
  	triangle(svg, object, x, y, (x + w), y, (x + w/2), (y + h));
}

function haxagon_shape(svg, object, x, y, w, h) {           
  	hexagon(svg, object, x, (y + h), (x + w/2), y, (x + w+w/2), y, (x + 2*w), (y + h), (x + w+w/2), (y + 2*h), (x + w/2), (y + 2*h));
}

function parallelogram_right(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + h), (x + w), (y + h), (x + w+w/2), y, (x + w/2), y);
}

function parallelogram_left(svg, object, x, y, w, h){
	rectangle(svg, object, x, y, (x + w), y, (x + w+w/2), (y+h), (x + w/2), (y + h));
}

function long_parallelogram_right(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + h), (x + 2*w), (y + h), (x + 2*w+w/2), y, (x + w/2), y);
}

function long_parallelogram_left(svg, object, x, y, w, h){
	rectangle(svg, object, x, y, (x + 2*w), y, (x + 2*w+w/2), (y + h), (x + w/2), (y+h));
}

function up_parallelogram(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + 2*h), (x + w), (y + 2*h), (x + 2*w), y, (x + w), y);
}

function diamond(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + h), (x + w/2), y, (x + w), (y + h), (x + w/2), (y + 2*h));
}

function trapezoid_down(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + h), (x + w/2), y, (x + w+w/2), y, (x + 2*w), (y + h));
}

function trapezoid_up(svg, object, x, y, w, h){
	rectangle(svg, object, x, (y + h), (x + w/2), (y + 2*h), (x + w+w/2), (y + 2*h), (x + 2*w), (y + h));
}

function arrow_right(svg, object, x, y, w, h) {           
  	hexagon(svg, object, x, y, (x + w), y, (x + w+w/2), (y + h), (x + w), (y + 2*h), x, (y + 2*h), (x + w/2), (y + h));
}

function arrow_left(svg, object, x, y, w, h) {           
  	hexagon(svg, object, x, (y + h), (x + w/2), y, (x + w+w/2), y, (x + w), (y + h), (x + w+w/2), (y + 2*h), (x + w/2), (y + 2*h));
}

function arrow_down(svg, object, x, y, w, h) {           
  	hexagon(svg, object, x, (y + h), (x + w/2), y, (x + w), (y + h), (x + 2*w), (y + h), (x + w+w/2), (y + 2*h), (x + w/2), (y + 2*h));
}

//---------------------------------- Drawing shapes ----------------------------------------
function add_shapes(object){

	var num = object.shape_number;
	var row = object.row;
	var col = object.column;

    let x = col * w;
    let y = row * h;
	
	switch (num) {
      case 0:
        triangle_up(svg, object, x, y, w, h)
        break
      case 1:
        triangle_down(svg, object, x, y, w, h)
        break
      case 2:
      	haxagon_shape(svg, object, x, y, w, h)
      	break
      case 3:
      	parallelogram_right(svg, object, x, y, w, h)
      	break
      case 4:
      	parallelogram_left(svg, object, x, y, w, h)
      	break	
      case 5:
      	long_parallelogram_right(svg, object, x, y, w, h)
      	break
      case 6:
      	long_parallelogram_left(svg, object, x, y, w, h)
      	break
      case 7:
      	up_parallelogram(svg, object, x, y, w, h)
      	break
      case 8:
      	diamond(svg, object, x, y, w, h)
      	break
      case 9:
      	trapezoid_down(svg, object, x, y, w, h)
      	break
      case 10:
      	trapezoid_up(svg, object, x, y, w, h)
      	break
      case 11:
      	arrow_right(svg, object, x, y, w, h)
      	break
      case 12:
      	arrow_left(svg, object, x, y, w, h)
      	break
      case 13:
      	arrow_down(svg, object, x, y, w, h)
      	break
    }
}

//--------------------------------- what's inside every shape ------------------------

function loadPage(object, shape){

	var active = 0;
	var playing = 0;
	var clicked = false;
	var delay_amount = 100;
	var audioPath = object.mp3;
	var subtitles = object.subtitles;
	var shape_color = object.after_colour;

	//Remove all elements from the page.
	d3.select("svg").remove();

	//Adding a div element to the body
    var page = d3.select("body")
			   	.append("div")
			    .attr("id", "page");

	var story = page.append("div")
		.attr("id", "buttons_div")
	    .attr('width', '300')
	  	.attr('height', '200');

	//Creating the buttons      
    var return_button = story.append("button");
    return_button.attr("id", "return_button")
			    .style("background-color", shape_color)
			    .append('text')
			    .html('<i class="fas fa-arrow-circle-left"></i>');

    var audio_button = story.append("button");
    audio_button.attr("id", "audio_button")
			    .style("background-color",shape_color)
			    .append('text')
			    .html('<i class="fas fa-volume-up"></i>');

    var cc_button = story.append("button");
	cc_button.attr("id", "subtitles_button")
			    .style("background-color",shape_color)
			    .append('text')
			    .html('<i class="fas fa-closed-captioning"></i>');

    
    //Writing the click function of each button
    //1. return button
	return_button.on('click', function(){
		window.location.reload();			
	});

	//2. audio button	
	audio_button.on('click', function(){

	});

	//3.subtitles button: cc_button
	cc_button.on('click', function(){
	    active += 1;
	    if (active == 1) {
			page.append("div")
				.attr("id", "sub_div")
				.style("background-color", shape_color)
				.text(subtitles)
				.style("padding", "15px")
				.style("color", "white");
		}
		else{
			d3.select("#sub_div").remove()
			active = 0;
		}
	});

	//add the shape
	var shape_svg = page.append("svg")
						.attr("id", "shape_svg")
						.attr("width", "600")
						.attr("height", "250");

	shape_svg.append("polygon")
			.attr("points", shape)
		  	.attr("fill", object.after_colour)
		  	.attr("stroke", "none")
		  	.attr("class", "poly");

	//Add the audio
	var mp3 = page.append("audio")
				.attr("id", "audioPlayer");
				
	var mp3_source = mp3.append("source")
						.attr("src", audioPath)
						.attr("type", "audio/mp3");

	//4. play button
	var play = page.append("button");
    	play.attr("id", "play_button")
		    .style("background-color", shape_color)
			.append('text')
		    .html('<i class="fas fa-play"></i>');

	play.on('click', function(){
		var audioElement = document.getElementById('audioPlayer');
		playing += 1;
		if (playing == 1) {	    	
		    audioElement.play();
		    play.html('<i class="fas fa-pause"></i>')
		}
		else{
		    audioElement.pause();
		    play.html('<i class="fas fa-play"></i>')
		    playing = 0;
		}
	});					
}