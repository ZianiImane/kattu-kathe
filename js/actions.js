
var width = 1000;
var height = 900;
var scale = 50;
var svg = d3.select('body')
  			.append('svg')
  			.attr('width', width)
  			.attr('height', height);

var area = d3.area() //M(x,y)
            .x(function (d, i) {return d.x;})//x
            .y0(height) //the lowest y we want to use.
            .y1(function (d) {return height - d.y;}); //y

var y = d3.scaleLinear().domain([0,360]).range([height, 0]);

var data = d3.json("data/data.json").then(function(data){
	var data_array = data;
	data_array.forEach(svg_add); 
});

function svg_add(object) {
	
    points = [];   
	object.point_position.forEach(function(value){
    	points.push({x: value.x, y: value.y});
    });

    svg.append(object.object_type)
        .attr('fill', object.before_colour)
        .attr('stroke', 'white')
        .attr('stroke-width', 0)
        .attr('d', area(points))
        .attr('transform', 'translate('+object.shape_id*2*scale+',0)')
        .on('mouseover', function() {
          	d3.select(this)
            .attr('stroke-width', 2)
        })
        .on('mouseout', function() {
            d3.select(this)
            .attr('stroke-width', 1)
        })
        .on('click', function(){
        	d3.select(this)
        	.style('fill', object.after_colour);

        	loadPage(object);	
        });

    console.log(d3.select(this));
};


function loadPage(object){
   		       		
    var saved_state = d3.select("svg"); 
    var shape = object;
    var shape_color = object.after_colour;
    var subtitles = object.subtitles;
    var active = 0;
    var audioPath = object.mp3;
    var playing = 0;
    var clicked = false;

    console.log(shape_color);

    d3.select("svg").remove(); 
    //Creating a new page with new elements
    var page = d3.select("body")
			    .append("div")
			    .attr("id", "page")

	var story = page.append("div")
		.attr("id", "buttons_div")
	    .attr('width', '100%')
	  	.attr('height', '100%');
    
    //Creating the three buttons      
    var return_button = story.append("button");
    return_button.attr("id", "return_button")
	    .style("background-color", shape_color)
	    .append('text')
	    .html('<i class="fas fa-arrow-circle-left"></i>');

	return_button.on('click', function(){
		window.location.reload();

			
	});

    story.append("button")
	    .attr("id", "audio_button")
	    .style("background-color",shape_color)
	    .append('text')
	    .html('<i class="fas fa-volume-up"></i>')
	    .on('click', function(){

	    

	    });

    var cc_button = story.append("button");

	cc_button.attr("id", "subtitles_button")
			    .style("background-color",shape_color)
			    .append('text')
			    .html('<i class="fas fa-closed-captioning"></i>');

	    cc_button.on('click', function(){
	    	active += 1;
	    	if (active == 1) {
			    page.append("div")
					.attr("id", "sub_div")
					.style("background-color", shape_color)
					.text(subtitles)
					.style("padding", "15px")
					.style("color", "white");
			}
			else{
				d3.select("#sub_div").remove()
				active = 0;
			}
	    });
	
	//Add the shape
	var shape_svg = page.append("svg")
					.attr("id", "shape_svg")
					.attr("width", "150px")
					.attr("height", "150px");
					
	shape_svg.append(object.object_type)
			        .attr('fill', object.after_colour)
			        .attr('stroke', 'white')
			        .attr('stroke-width', 0)
			        .attr('d', area(points));

	var play = page.append("button")
		.attr("id", "play_button")
	    .style("background-color", shape_color);

	play.append('text')
	    .html('<i class="fas fa-play"></i>');

	var mp3 = page.append("audio")
				.attr("id", "audioPlayer");
				
	var mp3_source = mp3.append("source")
						.attr("src", audioPath)
						.attr("type", "audio/mp3");

	play.on('click', function(){
		var audioElement = document.getElementById('audioPlayer')
			console.log(mp3_source);
			playing += 1;
			if (playing == 1) {	    	
		    	audioElement.play();
		    	play.html('<i class="fas fa-pause"></i>')
		    }
		    else{
		    	audioElement.pause();
		    	play.html('<i class="fas fa-play"></i>')
		    	playing = 0;
		    }
		  
		
	    

	    });


};





